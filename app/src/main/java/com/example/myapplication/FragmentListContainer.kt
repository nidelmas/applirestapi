package com.example.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.fragment.app.setFragmentResultListener

class FragmentListContainer : Fragment(), CustomAdapter.Listener {


    var buttonAdd : Button? = null
    var itemRecyclerView : RecyclerView? = null
    var arrayItems = mutableListOf<RootContainer>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setFragmentResultListener("testcle") { requestKey, bundle ->

            if (requestKey == "testcle") {
                val message = bundle.getString("bundleKey")
                if (message != null) {
                    val con = RootContainer()
                    arrayItems.add(con)

                    val test: CustomAdapter = itemRecyclerView?.adapter as CustomAdapter
                    test.update(arrayItems)

                }
            }

        }


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val ret = inflater.inflate(R.layout.fragment_list_to_do, container, false)

        //init list of root container
        val test:Requester = Requester()
        val testIfNull = test.getRootContainer()
        if (testIfNull != null){
            arrayItems = testIfNull
        }else{
            Toast.makeText(activity, "Host not found", Toast.LENGTH_SHORT).show()
        }

        //button pour ajouter un item
        buttonAdd = ret.findViewById(R.id.buttonAddItem)
        buttonAdd?.setOnClickListener{

            var transaction = parentFragmentManager.beginTransaction()

            transaction.add(R.id.fragmentContainerView,FragmentAddToDo())
            transaction.hide(this)
            transaction.commit()

        }

        //Recycler setup
        itemRecyclerView = ret.findViewById(R.id.recyclerView)
        itemRecyclerView?.layoutManager = LinearLayoutManager(activity)
        itemRecyclerView?.adapter = CustomAdapter(arrayItems,this)

        return ret

    }

    override fun onItemClicked(clickedView: View) {
        val pos = itemRecyclerView?.layoutManager?.getPosition(clickedView)
        val itemToSwapTo = arrayItems[pos!!]

        setFragmentResult("ContainerFragmentTag1", bundleOf("idParent" to itemToSwapTo.id))
        setFragmentResult("ContainerFragmentTag1", bundleOf("bundleKey" to itemToSwapTo.id.toString()))

        var transaction = parentFragmentManager.beginTransaction()

        transaction.add(R.id.fragmentContainerView,ContainerFragment(),"ContainerFragmentTag1")
        transaction.hide(this)
        transaction.addToBackStack("1")
        transaction.commit()

    }

}