package com.example.myapplication

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener


var backButton : Button? = null
var buttonAddNewItem : Button? = null
var editTextName : EditText? = null
var checkBoxContainer : CheckBox? = null
var checkBoxObject : CheckBox? = null

class FragmentAddToDo : Fragment() {

    var idItemParent : Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val ret = inflater.inflate(R.layout.fragment_add_to_do, container, false)

        setFragmentResultListener(this.tag!!) { requestKey, bundle ->

            if (requestKey == this.tag) {
                val message = bundle.getString("idParent")
               idItemParent = message?.toLong()
            }

        }

        backButton = ret.findViewById(R.id.backButtonAdd)
        buttonAddNewItem = ret.findViewById(R.id.buttonAddNewItem)
        editTextName = ret.findViewById(R.id.editTextName)
        checkBoxContainer = ret.findViewById(R.id.checkBoxContainer)
        checkBoxObject = ret.findViewById(R.id.checkBoxObject)

        checkBoxObject?.setOnClickListener{
            if(checkBoxObject?.isChecked!! && checkBoxContainer?.isChecked!!){
                checkBoxContainer?.isChecked = false
            }
        }
        checkBoxContainer?.setOnClickListener{
            if(checkBoxContainer?.isChecked!! && checkBoxObject?.isChecked!!){
                checkBoxObject?.isChecked = false
            }
        }

        buttonAddNewItem?.setOnClickListener{
            if(TextUtils.isEmpty(editTextName?.text.toString())){
                Toast.makeText(activity, "Le nom ne peut pas etre vide", Toast.LENGTH_SHORT).show()
            }else if(!checkBoxContainer?.isChecked!! && !checkBoxObject?.isChecked!!){
                Toast.makeText(activity, "Objet ou container ?", Toast.LENGTH_SHORT).show()
            }else{
                val textResult = editTextName?.text.toString()

                val req:Requester = Requester()
                if(checkBoxContainer?.isChecked!!){
                    val res = req.addContainer(textResult,idItemParent)
                }
                else{
                    val res = req.addObject(textResult,idItemParent)
                }
                var transaction = parentFragmentManager.beginTransaction()
                transaction.remove(this)
                transaction.commit()
                parentFragmentManager.popBackStack()
            }

        }

        return ret
    }



}