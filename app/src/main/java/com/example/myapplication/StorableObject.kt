package com.example.myapplication

class StorableObject {
    var id: Long? = null
    var name: String? = null
    var idContainer: Long? = null
    var idOwner: Long? = null
    var users: MutableList<User>? = null
    var tags : MutableList<Tag>? = null
    var path: String? = null
}