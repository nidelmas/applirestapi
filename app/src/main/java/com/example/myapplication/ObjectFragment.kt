package com.example.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.setFragmentResultListener

class ObjectFragment : Fragment() {

    var textview: TextView? = null
    var textviewPath: TextView? = null
    var textToDisplay: String? = null
    var textPathToDisplay: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener("idObjectToDisplay") { requestKey, bundle ->

            if (requestKey == "idObjectToDisplay") {
                val message = bundle.getString("bundleKey")
                if (message != null) {
                    val con = RootContainer()
                    val req:Requester = Requester()
                    val testIfNull = req.getObject(message.toInt())
                    if (testIfNull != null){
                        textToDisplay = testIfNull.name
                        textview!!.text = textToDisplay

                        textPathToDisplay = testIfNull.path
                        val delim = "/"
                        var pathDisplayed = ""
                        val list = textPathToDisplay?.split(delim)?.reversed()
                        pathDisplayed += list!![0]
                        var blanc = ""
                        for (i in 1 until list?.size!!){
                            for (j in 0..i){
                                blanc += "\t"
                            }
                            pathDisplayed += blanc + "\n" + blanc + "|\n" + blanc +"---" + list[i]
                        }
                        textviewPath!!.text = pathDisplayed
                    }

                }
            }

        }


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        val ret = inflater.inflate(R.layout.fragment_object, container, false)

        textview = ret.findViewById(R.id.textViewName)
        textview!!.text = textToDisplay

        textviewPath = ret.findViewById(R.id.textViewPath)

        return ret
    }


}