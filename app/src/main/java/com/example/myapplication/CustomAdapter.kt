package com.example.myapplication

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class CustomAdapter(private var dataSet: MutableList<RootContainer>, private val listener:Listener) :
    RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    interface Listener{
        fun onItemClicked(clickedView:View)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val titreTextView: TextView

        init {
            titreTextView = view.findViewById(R.id.titreTextView)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.list_cells, viewGroup, false)

        view.setOnClickListener{
            listener.onItemClicked(view)
        }

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        if (dataSet[position].name?.startsWith("Objet : ")!!) {
            viewHolder.titreTextView.setTextColor(Color.RED)
        }

        if (dataSet[position].name?.startsWith("Container : ")!!) {
            viewHolder.titreTextView.setTextColor(Color.BLUE)
        }
        viewHolder.titreTextView.text = dataSet[position].name
    }

    fun update(modelList:MutableList<RootContainer>){
        dataSet = modelList
        this!!.notifyDataSetChanged()
    }

    override fun getItemCount() = dataSet.size

}