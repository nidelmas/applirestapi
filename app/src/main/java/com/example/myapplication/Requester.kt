package com.example.myapplication

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import okhttp3.*
import org.json.JSONObject
import java.io.IOException

class Requester {

    private var client = OkHttpClient()
    var responseG: Response? = null

    init {
        this.client = client
    }

    fun getRootContainer(): MutableList<RootContainer>? {
        val t = Thread(Runnable {
            try {
                val request = Request.Builder()
                    .url("http://10.0.2.2:8080/api/container/rootContainer/${MainActivity.currentUser?.id}")
                    .header("Authorization", MainActivity.currentUser?.token)
                    .build()

                val call = client.newCall(request)
                var response = call.execute()
                if(!response.isSuccessful){
                    //Unsuccessful
                    throw IOException("Unexpected code $response")
                }
                responseG = response
            }
            catch (e: Exception){
                println("Thread Exception" + e.message)
            }
            finally {
                println("Thread finnaly")
            }
        })
        t.start()
        t.join()

        val response2 = responseG?.body()?.string()
        val sType = object : TypeToken<MutableList<RootContainer>>() { }.type
        val rootList = Gson().fromJson<MutableList<RootContainer>>(response2, sType)
        return rootList
    }

    fun getContainer(id:Int): RootContainer? {

        val t = Thread(Runnable {
            try {
                val request = Request.Builder()
                    .url("http://10.0.2.2:8080/api/container/$id")
                    .header("Authorization", MainActivity.currentUser?.token)
                    .build()

                val call = client.newCall(request)
                var response = call.execute()
                if(!response.isSuccessful){
                    //Unsuccessful
                    throw IOException("Unexpected code $response")
                }
                responseG = response
            }
            catch (e: Exception){
                println("Thread Exception" + e.message)
            }
            finally {
                println("Thread finnaly")
            }
        })
        t.start()
        t.join()

        val response2 = responseG?.body()?.string()
        val rootList = Gson().fromJson(response2, RootContainer::class.java)
        return rootList

    }

    fun getObject(id:Int): StorableObject? {

        val t = Thread(Runnable {
            try {
                val request = Request.Builder()
                    .url("http://10.0.2.2:8080/api/object/$id")
                    .header("Authorization", MainActivity.currentUser?.token)
                    .build()

                val call = client.newCall(request)
                var response = call.execute()
                if(!response.isSuccessful){
                    //Unsuccessful
                    throw IOException("Unexpected code $response")
                }
                responseG = response
            }
            catch (e: Exception){
                println("Thread Exception" + e.message)
            }
            finally {
                println("Thread finnaly")
            }
        })
        t.start()
        t.join()

        val response2 = responseG?.body()?.string()
        val objectRes = Gson().fromJson(response2, StorableObject::class.java)
        return objectRes

    }

    fun addObject(name: String, idContainer: Long?){
        val t = Thread(Runnable {
            try {
                var obj = StorableObject()
                obj.name = name
                obj.idOwner = MainActivity.currentUser?.id
                obj.idContainer = idContainer
                var tag = Tag()
                tag.name = "UnTagTestFront"
                obj.tags = mutableListOf<Tag>()
                obj.tags!!.add(tag)

                var gson = GsonBuilder().serializeNulls().setPrettyPrinting().create()
                println("GSON = " + gson.toJson(obj))

                val request = Request.Builder().url("http://10.0.2.2:8080/api/object").post(
                    RequestBody.create(MediaType.parse("application/json"), gson.toJson(obj))).header("Authorization", MainActivity.currentUser?.token).build()

                val call = client.newCall(request)
                var response = call.execute()
                if(!response.isSuccessful){
                    //Unsuccessful
                    throw IOException("Unexpected code $response")
                }

                responseG = response
                println("RES = " + response)
            }
            catch (e: Exception){
                println("Thread Exception " + e.message)
            }
            finally {
                println("Thread finnaly" + responseG?.body().toString())
            }
        })
        t.start()
        t.join()

    }

    fun addContainer(name: String, idContainer: Long?){
        val t = Thread(Runnable {
            try {
                var obj = RootContainer()
                obj.name = name
                obj.idOwner = MainActivity.currentUser?.id
                obj.idContainer = idContainer

                var gson = GsonBuilder().serializeNulls().setPrettyPrinting().create()
                println("GSON = " + gson.toJson(obj))

                val request = Request.Builder().url("http://10.0.2.2:8080/api/container").post(
                    RequestBody.create(MediaType.parse("application/json"), gson.toJson(obj))).header("Authorization", MainActivity.currentUser?.token).build()

                val call = client.newCall(request)
                var response = call.execute()
                if(!response.isSuccessful){
                    //Unsuccessful
                    throw IOException("Unexpected code $response")
                }

                responseG = response
                println("RES = " + response)
            }
            catch (e: Exception){
                println("Thread Exception " + e.message)
            }
            finally {
                println("Thread finnaly" + responseG?.body().toString())
            }
        })
        t.start()
        t.join()
    }

    fun getUser(id:Int): User? {
        val request = Request.Builder()
            .url("http://10.0.2.2:8080/api/user/$id")
            .build()

        val call = client.newCall(request)
        var response = call.execute()
        if (!response.isSuccessful){
            return null
        }
        val response2 = response.body()?.string()
        val rootList = Gson().fromJson(response2, User::class.java)
        return rootList

    }

    fun decoyGet(id:Int): MutableList<RootContainer>? {
        val falseJson = "[\n" +
                "    {\n" +
                "        \"id\": 3,\n" +
                "        \"name\": \"maison2\",\n" +
                "        \"idContainer\": null,\n" +
                "        \"owner\": null,\n" +
                "        \"containedPlaces\": null,\n" +
                "        \"containedObjects\": null\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 2,\n" +
                "        \"name\": \"maison1\",\n" +
                "        \"idContainer\": null,\n" +
                "        \"owner\": null,\n" +
                "        \"containedPlaces\": null,\n" +
                "        \"containedObjects\": null\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 4,\n" +
                "        \"name\": \"maison3\",\n" +
                "        \"idContainer\": null,\n" +
                "        \"owner\": null,\n" +
                "        \"containedPlaces\": null,\n" +
                "        \"containedObjects\": null\n" +
                "    }\n" +
                "]"
        val sType = object : TypeToken<MutableList<RootContainer>>() { }.type
        val rootList = Gson().fromJson<MutableList<RootContainer>>(falseJson, sType)
        return rootList
    }

    fun decoyGet2(id:Int): RootContainer? {
        val falseJson = "{\n" +
                "    \"id\": 2,\n" +
                "    \"name\": \"maison1\",\n" +
                "    \"idContainer\": null,\n" +
                "    \"owner\": {\n" +
                "        \"id\": 1,\n" +
                "        \"name\": \"user\",\n" +
                "        \"password\": null\n" +
                "    },\n" +
                "    \"containedPlaces\": [\n" +
                "        {\n" +
                "            \"id\": 5,\n" +
                "            \"name\": \"chambre1\",\n" +
                "            \"idContainer\": 2,\n" +
                "            \"owner\": null,\n" +
                "            \"containedPlaces\": null,\n" +
                "            \"containedObjects\": null\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 6,\n" +
                "            \"name\": \"chambre2\",\n" +
                "            \"idContainer\": 2,\n" +
                "            \"owner\": null,\n" +
                "            \"containedPlaces\": null,\n" +
                "            \"containedObjects\": null\n" +
                "        }\n" +
                "    ],\n" +
                "    \"containedObjects\": [\n" +
                "        {\n" +
                "            \"id\": 7,\n" +
                "            \"name\": \"livre1\",\n" +
                "            \"idContainer\": 2,\n" +
                "            \"owner\": null,\n" +
                "            \"users\": null,\n" +
                "            \"tags\": [\n" +
                "                {\n" +
                "                    \"id\": 10,\n" +
                "                    \"name\": \"tag2\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"id\": 9,\n" +
                "                    \"name\": \"tag1\"\n" +
                "                }\n" +
                "            ]\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 8,\n" +
                "            \"name\": \"livre2\",\n" +
                "            \"idContainer\": 2,\n" +
                "            \"owner\": null,\n" +
                "            \"users\": null,\n" +
                "            \"tags\": []\n" +
                "        }\n" +
                "    ]\n" +
                "}"
        val rootList = Gson().fromJson(falseJson, RootContainer::class.java)
        return rootList
    }


}