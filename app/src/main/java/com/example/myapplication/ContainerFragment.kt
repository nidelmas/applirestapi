package com.example.myapplication

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.fragment.app.setFragmentResultListener

class ContainerFragment : Fragment(), CustomAdapter.Listener {


    var buttonAdd : Button? = null
    var itemRecyclerView : RecyclerView? = null
    var arrayItems = mutableListOf<RootContainer>()
    var idItemParent : Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }

        setFragmentResultListener(this.tag!!) { requestKey, bundle ->

            if (requestKey == this.tag) {
                val message = bundle.getString("bundleKey")
                idItemParent = message?.toLong()
                if (message != null) {
                    val con = RootContainer()
                    val req:Requester = Requester()
                    val testIfNull = req.getContainer(message.toInt())
                    if (testIfNull != null){
                        for (i in testIfNull.containedObjects!!){
                            i.name = "Objet : " + i.name
                        }
                        for (i in testIfNull.containedPlaces!!){
                            i.name = "Container : " + i.name
                        }
                        arrayItems = testIfNull.containedObjects!!
                        testIfNull.containedPlaces?.let { arrayItems.addAll(it) }
                    }

                    val test: CustomAdapter = itemRecyclerView?.adapter as CustomAdapter
                    test.update(arrayItems)

                }
            }

        }


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val ret = inflater.inflate(R.layout.fragment_list_to_do, container, false)

        buttonAdd = ret.findViewById(R.id.buttonAddItem)

        //Recycler setup
        itemRecyclerView = ret.findViewById(R.id.recyclerView)
        itemRecyclerView?.layoutManager = LinearLayoutManager(activity)
        itemRecyclerView?.adapter = CustomAdapter(arrayItems,this)


        buttonAdd?.setOnClickListener{

            var transaction = parentFragmentManager.beginTransaction()

            setFragmentResult("FragAddToDo", bundleOf("idParent" to idItemParent.toString()))

            transaction.add(R.id.fragmentContainerView,FragmentAddToDo(), "FragAddToDo")
            transaction.hide(this)
            transaction.commit()

        }

        return ret

    }

    override fun onItemClicked(clickedView: View) {
        val pos = itemRecyclerView?.layoutManager?.getPosition(clickedView)
        val itemToSwapTo = arrayItems[pos!!]
        this.idItemParent = itemToSwapTo.id
        if(itemToSwapTo.name?.length!! > 11) {
            if (itemToSwapTo.name?.startsWith("Container :")!!) {
                setFragmentResult(
                    this.tag + "1",
                    bundleOf("bundleKey" to itemToSwapTo.id.toString())
                )

                var transaction = parentFragmentManager.beginTransaction()

                transaction.add(R.id.fragmentContainerView, ContainerFragment(), this.tag + "1")
                transaction.hide(this)
                transaction.addToBackStack("1")
                transaction.commit()
            } else {
                setFragmentResult(
                    "idObjectToDisplay",
                    bundleOf("bundleKey" to itemToSwapTo.id.toString())
                )

                var transaction = parentFragmentManager.beginTransaction()

                transaction.add(R.id.fragmentContainerView, ObjectFragment(), "objectFragment")
                transaction.hide(this)
                transaction.addToBackStack("1")
                transaction.commit()
            }
        }

    }

}