package com.example.myapplication

import java.io.Serializable

class User: Serializable{
    var id: Long? = null
    var name: String? = null
    var password: String? = null
    var token: String? = null
}