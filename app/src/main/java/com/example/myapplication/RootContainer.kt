package com.example.myapplication

class RootContainer() {
    var id: Long? = null
    var name: String? = null
    var idContainer: Long? = null
    var idOwner: Long? = null
    var containedPlaces: MutableList<RootContainer>? = null
    var containedObjects : MutableList<RootContainer>? = null

    override fun toString(): String {
        return "id : $id \nname : $name \nidContainer : $idContainer \nidOwner : $idOwner \n"
    }


}