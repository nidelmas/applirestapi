package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.*
import java.io.IOException

class ConnectionActivity : AppCompatActivity() {

    var buttonConexion: Button?= null
    var idText: EditText?= null
    var passwordText: EditText?= null

    private var client = OkHttpClient()
    var responseG: Response? = null
    var user: User? = null

    init {
        this.client = client
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_connection)

        idText = findViewById<EditText>(R.id.editTextIdentifiant)
        passwordText = findViewById<EditText>(R.id.editTextPassword)

        buttonConexion = findViewById<Button>(R.id.buttonConnexion)
        buttonConexion?.setOnClickListener{
            val t = Thread(Runnable {
                try {
                    val requestBody: RequestBody = FormBody.Builder().add("user", idText?.text.toString()).add("password", passwordText?.text.toString()).build()

                    val request = Request.Builder().url("http://10.0.2.2:8080/api/signin").post(requestBody).build()

                    val call = client.newCall(request)
                    var response = call.execute()
                    if(!response.isSuccessful){
                        //Unsuccessful
                        throw IOException("Unexpected code $response")
                    }

                    responseG = response
                    val response2 = responseG?.body()?.string()
                    val sType = object : TypeToken<User>(){}.type
                    user = Gson().fromJson<User>(response2, sType)
                    user?.token = responseG?.header("Authorization")
                }
                catch (e: Exception){
                    println("Thread Exception " + e.message)
                }
                finally {
                    println("Thread finnaly" + responseG?.body().toString())
                }
            })
            t.start()
            t.join()

            println("User : " + user?.id + " " + user?.name + " " + user?.token)

            if(user?.id != null){
                val intent = Intent(this, MainActivity::class.java).putExtra("user", user)
                startActivity(intent)
            }
        }
    }
}