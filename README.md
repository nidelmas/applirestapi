Projet etude de cas Web et Mobile de Master 2

réalisé par :
- Gabriel EYCHENE
- Guillaume COTHENET
- Mouhamadou GUEYE
- Nicolas DELMAS
- Sarra BENLOULOU HADJI

Partie Mobile

Créé avec android studio, le code a été écrit en Kotlin.
Les appels réseaux sont fait avec le bibliotheque OkHttp3 et le parsage du Json avec Gson.

Les fichiers de code ce trouve dans : applirestapi/app/src/main/java/com/example/myapplication

- ConnectionActivity.kt : Première activitée de l'application qui demande a l'utilisateur de ce connecter.
- ContainerFragment.kt : Le fragment qui affiche la liste des containers il a un setFragmentResultListener qui lui permet d'obtenir l'id du container a ouvrir. La méthode onItemClicked permet d'ouvrir a nouveau ce fragment avec l'id du container cliqué.
- CustomAdapter.kt : Classe qui permet de creer les Recycled view.
- FragmentAddContainer.kt : Fragment qui permet d'ajouter un nouveau Container/Objet
- FragmentListContainer.kt : Fragment initial ouvert apres la connexion qui affiche les container racine de l'utilisateur.
- MainActivity.kt : Activity qui lance tout les fragment en rapport avec les Container.
- ObjectFragment.kt : Fragment ouvert quand un objet qui n'est pas un Container est cliqué, il permet d'afficher le chemin complet de l'objet.
- Requester.kt : Classe ou tout les appels réseaux sont fait.
